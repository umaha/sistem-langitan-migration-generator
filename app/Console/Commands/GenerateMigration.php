<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\Closure;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\PsrPrinter;

class GenerateMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:generate-migration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Sistem Langitan DB Migration for PostgreSQL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tables = [];

        // Get All Tables
        $user_tables = DB::table('user_tables')->select('table_name')
            //->where('table_name', 'like', 'LOG_%')
            ->orderBy('table_name')->get();

        foreach ($user_tables as $user_table) {
            $this->line('Collect info ' . $user_table->table_name);

            $table = [];
            $table['table_name'] = $user_table->table_name;
            $table['cols'] = [];

            // Ambil kolom pada tabel
            $user_tab_cols = DB::table('user_tab_cols')
                ->where('table_name', $user_table->table_name)
                ->where('hidden_column', 'NO')
                ->orderBy('column_id')->get();

            foreach ($user_tab_cols as $user_tab_col) {
                array_push($table['cols'], [
                    'column_id'         => $user_tab_col->column_id,
                    'column_name'       => $user_tab_col->column_name,
                    'data_type'         => $user_tab_col->data_type,
                    'data_length'       => $user_tab_col->data_length,
                    'data_precision'    => $user_tab_col->data_precision,
                    'data_scale'        => $user_tab_col->data_scale,
                    'nullable'          => $user_tab_col->nullable,
                    'data_default'      => $user_tab_col->data_default
                ]);
            }

            array_push($tables, $table);

            $user_tab_cols = null;
        }

        $urutan = 1;

        foreach ($tables as $table) {

            $phpFile = new PhpFile();
            $phpFile
                ->addUse('Illuminate\Database\Migrations\Migration')
                ->addUse('Illuminate\Database\Schema\Blueprint')
                ->addUse('Illuminate\Support\Facades\Schema');

            $className = $this->tableNameToClassName($table['table_name']);
            $class = $phpFile->addClass($className);
            $class->addExtend('Migration');

            // Create Table Closure
            $tableBlueprint = new Closure();
            $tableBlueprint->addParameter('table')->setType('Blueprint');

            // Columns
            foreach ($table['cols'] as $col) {

                $col_type = "undefined('{$col['column_name']}')";
                $col_data_default = "";

                // NUMBER
                if ($col['data_type'] == 'NUMBER') {
                    if ($col['data_scale'] == null) {
                        $col_type = "integer('{$col['column_name']}')";
                    } elseif ($col['data_scale'] == 0) {
                        if ($col['data_precision'] == null) {
                            $col_type = "integer('{$col['column_name']}')";
                        } elseif ($col['data_precision'] > 9) {
                            $col_type = "bigInteger('{$col['column_name']}')";
                        } elseif ($col['data_precision'] > 4) {
                            $col_type = "integer('{$col['column_name']}')";
                        } else {
                            $col_type = "smallInteger('{$col['column_name']}')";
                        }
                    } else {
                        $col_type = "decimal('{$col['column_name']}', {$col['data_precision']}, {$col['data_scale']})";
                    }

                    if (is_numeric($col['data_default'])) {
                        $col_data_default = "->default({$col['data_default']})";
                    }
                }

                // FLOAT
                if ($col['data_type'] == 'FLOAT') {
                    $col_type = "float('{$col['column_name']}')";

                    if ($col['data_default'] != null) {
                        $col_data_default = "->default({$col['data_default']})";
                    }
                }

                // VARCHAR2
                if ($col['data_type'] == 'VARCHAR2' || $col['data_type'] == 'NVARCHAR2') {
                    $col_type = "string('{$col['column_name']}', {$col['data_length']})";

                    if ($col['data_default'] != null && $col['data_default'] != 'NULL') {
                        $col_data_default = "->default({$col['data_default']})";
                    }
                }

                // CLOB
                if ($col['data_type'] == 'CLOB') {
                    $col_type = "text('{$col['column_name']}')";
                }

                // BLOB
                if ($col['data_type'] == 'BLOB') {
                    $col_type = "binary('{$col['column_name']}')";
                }

                // CHAR
                if ($col['data_type'] == 'CHAR') {
                    $col_type = "char('{$col['column_name']}', {$col['data_length']})";

                    if ($col['data_default'] != null && $col['data_default'] != 'NULL') {
                        $col_data_default = "->default({$col['data_default']})";
                    }
                }

                // DATE
                if ($col['data_type'] == 'DATE') {
                    $col_type = "dateTime('{$col['column_name']}')";

                    if (in_array(strtoupper(trim($col['data_default'])), ['SYSDATE', 'CURRENT_DATE'])) {
                        $col_data_default = "->useCurrent()";
                    }
                }

                // TIMESTAMP
                if (substr($col['data_type'], 0, 9) == 'TIMESTAMP') {
                    $col_type = "timestamp('{$col['column_name']}', {$col['data_scale']})";

                    if (in_array(strtoupper(trim($col['data_default'])), ['SYSDATE', 'CURRENT_TIMESTAMP'])) {
                        $col_data_default = "->useCurrent()";
                    }
                }

                // NULLABLE
                $col_nullable = $col['nullable'] == 'Y' ? "->nullable()" : "";

                // Special Column : CREATED_ON
                if ($col['column_name'] == 'CREATED_ON') {
                    $col_data_default = '->useCurrent()';
                }
                // Special Column : UPDATED_ON
                if ($col['column_name'] == 'UPDATED_ON') {
                    $col_data_default = '->useCurrent()->useCurrentOnUpdate()';
                }

                $tableBlueprint->addBody("\$table->$col_type$col_nullable$col_data_default;");
            }

            $class->addMethod('up')
                ->addBody("Schema::connection('pgsql')->create('{$table['table_name']}', $tableBlueprint);");

            $class->addMethod('down')
                ->setBody("Schema::dropIfExists('{$table['table_name']}');");

            $this->info($this->tableNameToFileName($table['table_name'], $urutan));

            $printer = new PsrPrinter();
            $fileHandle = fopen('database/migrations/' . $this->tableNameToFileName($table['table_name'], $urutan), 'w');
            fwrite($fileHandle, $printer->printFile($phpFile));
            fclose($fileHandle);

            $urutan++;
        }
    }

    private function tableNameToClassName($tableName)
    {
        return 'Create' . str_replace(' ', '', ucwords(strtolower(str_replace('_', ' ', $tableName)))) . 'Table';
    }

    private function tableNameToFileName($tableName, $urutan = '0')
    {
        return date('Y_00_00_') . str_pad($urutan, 6, '0', STR_PAD_LEFT) . '_create_' . strtolower($tableName) . '_table.php';
    }
}
